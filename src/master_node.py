#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def handle_data_received(data):
  rospy.loginfo("data_received: %s", data.data)

def master_main():
  rospy.init_node('listener', anonymous=True)
  rospy.Subscriber('data_received', String, handle_data_received)
  rospy.spin()

if __name__ == '__main__':
  master_main()
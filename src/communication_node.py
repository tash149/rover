#!/usr/bin/env python

from flask import Flask
import threading

import rospy
from std_msgs.msg import String

server = Flask(__name__)

# Server Functions

@server.route('/')
def hello_world():
  return 'Hello, World'

def shutdown_server():
  func = flask_request.environ.get('werkzeug.server.shutdown')
  if func is None:
      raise RuntimeError('Not running with the Werkzeug Server')
  func()

def start_server():
  server.run(host='127.0.0.1', port='3000', debug=False)

# ----------------

# ROS Functions

def read_comms(msg_id):
  msg = '<attribute> ' + str(msg_id)
  msg_id = msg_id + 1
  return msg

def comm_main():
  pub_data_received = rospy.Publisher('data_received', String, queue_size=10)
  rospy.init_node('communication_node', anonymous=True)
  rate = rospy.Rate(10) # 10hz
  
  msg_id = 0
  while not rospy.is_shutdown():
    msg = read_comms(msg_id)
    rospy.loginfo(msg)
    pub_data_received.publish(msg)
    rate.sleep()
    
    msg_id = msg_id + 1

# ----------------

if __name__ == '__main__':
  server_thread = threading.Thread(target=start_server, args=())
  server_thread.start()

  try:
    comm_main()
  except rospy.ROSInterruptException:
    pass
  
  server_thread.join()

# rover
This repository will contain all the nodes deployed on rover.

---
# Nodes to run

* Communication Node: `rosrun rover communication_node.py`
* Master Node: `rosrun rover master`
In order to kill the node of communication node, close the terminal. `Ctrl+C` won't work.

---

# Nodes

## Communication Node

| Publishes To | Subscribes To |
| ------------ | ------------- |
| data_received | show_gps |
| | show_lidar |
| | show_gps |
| | show_lidar |
| | show_ultrasonic |
| | show_dashcam |
| | show_armcam |

## Master Node

### Navigation commands

| Publishes To |
| ------------ |
| move_fd |
| move_bk |
| move_lt0 |
| move_rt0 |
| move_lt1 | 
| move_lt2 |

### Rover-BaseStation Communication

| Publishes To | Subscribes To |
| ------------ | ------------- |
| show_lidar | data_received |
| show_gps | |
| show_ultrasonic | |
| show_dashcam | |
| show_armcam | |

## Maneuver Node

| Subscribes To |
| ------------ |
| move_fd |
| move_bk |
| move_lt0 |
| move_rt0 |
| move_lt1 | 
| move_lt2 |

---

## Sensor Node

| Publishes To |
| ------------ |
| sensor_gps |
| sensor_ultrasonic |
| sensor_lidar |

# Topics

### BaseStation-Rover

| Topic Name  | Value | Description |
| ------------- | ------------- | ------------- | 
| data_received  | JSON string  | Command sent by base station |
| show_gps | bool | start/stop sending GPS data to base station |
| show_lidar | bool | start/stop sending LIDAR value to base station |
| show_ultrasonic | bool | start/stop sending Ultrasonic values to base station |
| show_dashcam | TODO | start/stop sending dashcam feed to base station |
| show_armcam | TODO | start/stop sending armcam feed to base station |

### Maneuvering

| Topic Name  | Value | Description |
| ------------- | ------------- | ------------- | 
| move_fd | int | move forwards at given speed |
| move_bk | int | move backwards at given speed |
| move_lt0 | float | standing left-rotation by given angle |
| move_rt0 | float | standing right-rotation by given angle |
| move_lt1 | float | left turn while moving forward by given angle |
| move_rt1 | float | right turn while moving forward |

### Sensors

| Topic Name  | Value | Description |
| ------------- | ------------- | ------------- | 
| sensor_gps | TODO | read gps value from hardware |
| sensor_ultrasonic | TODO | read ultrasonic value from hardware |
| sensor_lidar | TODO | read lidar value from hardware |

JSON string associated with `data_received` is of the following format:

```
{
	command: string,
	value: any
}
```
